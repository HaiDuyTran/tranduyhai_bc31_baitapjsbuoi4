/**
Input : Thành viên 

Các bước xử lý
Dùng if else if
-IF  B : chào Bố ạ
else if M: chào Mẹ ạ
else if A: chòa Anh ạ
else if E:chào em gái
else :chào người lạ

Output:
Lời chào tương ứng với mỗi thành viên
 */

function loiChao() {
  var thanhVienValue = document.getElementById("txt-thanh-vien").value * 1;
  if (thanhVienValue == 0.1) {
    console.log("chào bố ạ", thanhVienValue);
   document.getElementById("loiChao").innerHTML="chào bố ạ"
  } else if (thanhVienValue == 0.2) {
    console.log("chào mẹ ạ");
    document.getElementById("loiChao").innerHTML="chào mẹ ạ"
  } else if (thanhVienValue == 0.3) {
    console.log("chào anh ạ");
    document.getElementById("loiChao").innerHTML="chào anh trai ạ"
  } else if (thanhVienValue == 0.4) {
    console.log("chào em gái");
    document.getElementById("loiChao").innerHTML="chào em gái"
  } else {
    console.log("chào người lạ");
    document.getElementById("loiChao").innerHTML="chào người lạ"
  }
}
