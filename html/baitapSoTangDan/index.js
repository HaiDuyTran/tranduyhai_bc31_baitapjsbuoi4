/**
Input:Nhập 3 số nguyên

Các bước xử lý
-tạo 3 biến lưu giá trị
-    If so1>so2 && so2>so3 => so1>so2>so3
else if so1>so3 && so3>so2 => so1>so2>so3
else if so2>so1 && so1>so3 => so2>so1>so3
else if so2>so3 && so3>so1 => so2>so3>so1
else if so3>so2 && so2>so1 => so3>so2>so1
else if so3>so1 && so1>so2 => so3>so1>so2

Output
Số tăng dần
 */
function soTangDan() {
  console.log("test");
  var soNguyen1Value = document.getElementById("txt-so-1").value * 1;
  var soNguyen2Value = document.getElementById("txt-so-2").value * 1;
  var soNguyen3Value = document.getElementById("txt-so-3").value * 1;
  if (soNguyen1Value > soNguyen2Value && soNguyen2Value > soNguyen3Value) {
    console.log("so1>so2>so3");
    document.getElementById("so-1").innerHTML =
      `<div> ` + soNguyen1Value + ` > </div>`;
    document.getElementById("so-2").innerHTML =
      `<div> ` + soNguyen2Value + ` > </div>`;
    document.getElementById("so-3").innerHTML =
      `<div> ` + soNguyen3Value + ` </div>`;
  } else if (
    soNguyen1Value > soNguyen3Value &&
    soNguyen3Value > soNguyen2Value
  ) {
    console.log("so1>so3>so2");
    document.getElementById("so-1").innerHTML =
      `<div> ` + soNguyen1Value + ` > </div>`;
    document.getElementById("so-2").innerHTML =
      `<div> ` + soNguyen3Value + ` > </div>`;
    document.getElementById("so-3").innerHTML =
      `<div> ` + soNguyen2Value + `  </div>`;
  } else if (
    soNguyen2Value > soNguyen1Value &&
    soNguyen1Value > soNguyen3Value
  ) {
    console.log("so2>so1>so3");
    document.getElementById("so-1").innerHTML =
      `<div> ` + soNguyen2Value + ` > </div>`;
    document.getElementById("so-2").innerHTML =
      `<div> ` + soNguyen1Value + ` > </div>`;
    document.getElementById("so-3").innerHTML =
      `<div> ` + soNguyen3Value + `  </div>`;
  } else if (
    soNguyen2Value > soNguyen3Value &&
    soNguyen3Value > soNguyen1Value
  ) {
    console.log("so2>so3>so1");
    document.getElementById("so-1").innerHTML =
      `<div> ` + soNguyen2Value + ` > </div>`;
    document.getElementById("so-2").innerHTML =
      `<div> ` + soNguyen3Value + ` > </div>`;
    document.getElementById("so-3").innerHTML =
      `<div> ` + soNguyen1Value + `  </div>`;
  } else if (
    soNguyen3Value > soNguyen2Value &&
    soNguyen2Value > soNguyen1Value
  ) {
    console.log("so3>so2>so1");
    document.getElementById("so-1").innerHTML =
      `<div> ` + soNguyen3Value + ` > </div>`;
    document.getElementById("so-2").innerHTML =
      `<div> ` + soNguyen2Value + ` > </div>`;
    document.getElementById("so-3").innerHTML =
      `<div> ` + soNguyen1Value + `  </div>`;
  } else if (
    soNguyen3Value > soNguyen1Value &&
    soNguyen1Value > soNguyen2Value
  ) {
    console.log("so3>so1>so2");
    document.getElementById("so-1").innerHTML =
      `<div> ` + soNguyen3Value + ` > </div>`;
    document.getElementById("so-2").innerHTML =
      `<div> ` + soNguyen1Value + ` > </div>`;
    document.getElementById("so-3").innerHTML =
      `<div> ` + soNguyen2Value + `  </div>`;
  }
}
