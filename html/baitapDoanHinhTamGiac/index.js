/**
Input:Nhập độ dài ba cạnh hình tam giác

Các bước xử lý
-tạo biến lưu giá trị 3 cạnh
-If 3 cạnh bằng nhau =>tam giác đều
-else if canh1*canh1+canh2*canh2=canh3*canh3 =>tam giác vuông
-else if canh1=canh2 &&canh1=!canh3 =>tam giác cân
   
Output
Tam giác này là tam giác ?
 */
function tamGiac() {
  var canh1Value = document.getElementById("txt-canh-1").value * 1;
  var canh2Value = document.getElementById("txt-canh-2").value * 1;
  var canh3Value = document.getElementById("txt-canh-3").value * 1;
  if (canh1Value == canh2Value && canh2Value == canh3Value) {
    console.log("Tam giác đều");
    document.getElementById("tam-giac").innerHTML = "Tam giác đều";
  } else if (
    canh1Value == canh2Value ||
    canh2Value == canh3Value ||
    canh1Value == canh3Value
  ) {
    console.log("Tam giác cân");
    document.getElementById("tam-giac").innerHTML = "Tam giác cân";
  } else if (
    canh1Value * canh1Value + canh2Value * canh2Value ==
      canh3Value * canh3Value ||
    canh1Value * canh1Value + canh3Value * canh3Value ==
      canh2Value * canh2Value ||
    canh3Value * canh3Value + canh2Value * canh2Value == canh1Value * canh1Value
  ) {
    console.log("Tam giác vuông");
    document.getElementById("tam-giac").innerHTML = "Tam giác vuông";
  } else {
    console.log("Tam giác nào đó");
    document.getElementById("tam-giac").innerHTML = "Tam giác nào đó";
  }
}
